import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { getMessagesConfig } from '../../../utilities/form.utils';
import { UserDataService } from '../../../services/data/user.data.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.scss'],
    providers: [ UserDataService ]
})
export class ContactComponent implements OnInit
{
    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any = [];

    constructor(
        private userDataService: UserDataService,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit(): void
    {
        //init form
        this.form = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.email]),
            subject: new FormControl(null, [Validators.required, Validators.maxLength(200)]),
            message: new FormControl(null, [Validators.required, Validators.maxLength(500)])
        });

        //messages config
        const fieldsConfig = {
            email: ['required', 'email'],
            subject: ['required', { key: "maxlength", value: 200}],
            message: ['required', { key: "maxlength", value: 500}],
        };
        this.messages = getMessagesConfig(fieldsConfig);
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.userDataService
            .contactMessage(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.snackBar.open('Successfully sent message!');
                    this.form.reset()
                },
                () =>
                {
                    this.inProgress = false;
                });
    }
}
