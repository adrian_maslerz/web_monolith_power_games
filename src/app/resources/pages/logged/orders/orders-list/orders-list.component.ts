import { Component, OnInit } from '@angular/core';
import { OrdersDataService } from '../../../../../services/data/orders.data.service';
import { Order } from '../../../../../interfaces/order.interface';

@Component({
    selector: 'app-orders-list',
    templateUrl: './orders-list.component.html',
    styleUrls: ['./orders-list.component.scss']
})
export class OrdersListComponent implements OnInit
{
    public orders: Order[] = [];
    public total: number = 0;
    public page: number = 1;
    public pages: number = 0;
    public sort: string = "createdDESC";
    public readonly results: number = 10;

    public table: OrdersListComponent;

    constructor(
        private ordersDataService: OrdersDataService
    ) { }

    ngOnInit(): void
    {
        this.page = 1;
        this.getData();
    }

    public onPageChange(page): void
    {
        this.page = page;
        this.getData();
    }

    public onSortChange(sort: string): void
    {
        this.sort = sort;
        this.page = 1;
        this.getData();
    }

    private getData(): void
    {
        this.ordersDataService
            .getOrders({
                page: this.page,
                sort: this.sort,
                results: this.results
            })
            .subscribe(results => {
                this.total = results.total;
                this.pages = results.pages;
                this.orders = results.results;
            })
    }
}
