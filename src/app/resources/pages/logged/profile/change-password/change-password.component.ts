import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserDataService } from '../../../../../services/data/user.data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { equalToFieldValue, password } from '../../../../../utilities/validators';
import { getMessagesConfig } from '../../../../../utilities/form.utils';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit
{
    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any[] = [];

    constructor(
        private router: Router,
        private userDataService: UserDataService,
        private snackBar: MatSnackBar
    ) {}

    ngOnInit(): void
    {
        //init form
        this.form = new FormGroup({
            old_password: new FormControl(null, [Validators.required]),
            new_password: new FormControl(null, [Validators.required, Validators.minLength(8), password]),
            repeated_password: new FormControl(null, [Validators.required]),
        });

        //messages config
        const fieldsConfig = {
            old_password: ['required'],
            new_password: ['required', { key: 'minlength', value: 8 }, 'password', { key: 'equalToFieldValue', message: 'Passwords don\'t match.' }],
            repeated_password: ['required', { key: 'equalToFieldValue', message: 'Passwords don\'t match.' }]
        };
        this.messages = getMessagesConfig(fieldsConfig);

        // password
        this.form.get('new_password')
            .valueChanges
            .subscribe(
                () =>
                {
                    const control = this.form.get('repeated_password');
                    control.setValidators([Validators.required, equalToFieldValue(this.form.get('new_password').value)]);
                    control.updateValueAndValidity();
                });

        this.form.get('repeated_password')
            .valueChanges
            .subscribe(() => this.form.get('repeated_password').setValidators([Validators.required, equalToFieldValue(this.form.get('new_password').value)]));
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.userDataService
            .changePassword(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.router.navigate(["/logged/profile"]);
                    this.snackBar.open('Password changed successfully!');
                },
                () =>
                {
                    this.inProgress = false;
                });
    }
}
