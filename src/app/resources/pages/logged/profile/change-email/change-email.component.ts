import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserDataService } from '../../../../../services/data/user.data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { getMessagesConfig } from '../../../../../utilities/form.utils';

@Component({
    selector: 'app-change-email',
    templateUrl: './change-email.component.html',
    styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent implements OnInit
{
    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any[] = [];

    constructor(
        private router: Router,
        private userDataService: UserDataService,
        private snackBar: MatSnackBar
    ) {}

    ngOnInit(): void
    {
        //init form
        this.form = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.email]),
            password: new FormControl(null, [Validators.required]),
        });

        //messages config
        const fieldsConfig = {
            email: ['required', 'email'],
            password: ['required'],
        };
        this.messages = getMessagesConfig(fieldsConfig);
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.userDataService
            .changeEmail(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.router.navigate(["/logged/profile"]);
                    this.snackBar.open('Email changed successfully!');
                },
                () =>
                {
                    this.inProgress = false;
                });
    }
}
