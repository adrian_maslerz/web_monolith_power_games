import { Component, OnInit } from '@angular/core';
import { UserDataService } from '../../../../services/data/user.data.service';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    providers: [ UserDataService ]
})
export class ProfileComponent
{
    constructor() { }
}
