import { Component, OnInit } from '@angular/core';
import { User } from '../../../../../interfaces/user.interface';
import { UserDataService } from '../../../../../services/data/user.data.service';

@Component({
    selector: 'app-profile-details',
    templateUrl: './profile-details.component.html',
    styleUrls: ['./profile-details.component.scss']
})
export class ProfileDetailsComponent implements OnInit
{

    public user: User;
    constructor(private userDataService: UserDataService) {}

    ngOnInit()
    {
        //populating data
        this.userDataService.getUser().subscribe((user: User) => this.user = user);
    }

}
