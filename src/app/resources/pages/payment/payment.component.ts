import { Component, OnInit, ViewChild } from '@angular/core';
import { CartElement } from '../../../interfaces/cart-element.interface';
import { Subscription } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CartStateService } from '../../../services/state/cart.state.service';
import { Router } from '@angular/router';
import { getMessagesConfig } from '../../../utilities/form.utils';
import { phoneNumber } from '../../../utilities/validators';
import { UserDataService } from '../../../services/data/user.data.service';
import { User } from '../../../interfaces/user.interface';
import { Customer } from '../../../interfaces/customer.interface';
import { PaymentsDataService } from '../../../services/data/payments.data.service';
import { flatMap } from 'rxjs/operators';
import { StripeCardComponent, StripeService } from 'ngx-stripe';
import { StripeCardElementOptions, StripeElementsOptions } from '@stripe/stripe-js';
import { Card } from '../../../interfaces/card.interface';
import { MatSnackBar } from '@angular/material/snack-bar';
import { OrdersDataService } from '../../../services/data/orders.data.service';

@Component({
    selector: 'app-payment',
    templateUrl: './payment.component.html',
    styleUrls: ['./payment.component.scss'],
    providers: [ UserDataService, PaymentsDataService, OrdersDataService ]
})
export class PaymentComponent implements OnInit {

    @ViewChild(StripeCardComponent) card: StripeCardComponent;
    public elementsOptions: StripeElementsOptions = {
        locale: 'en'
    };

    public cardOptions: StripeCardElementOptions = {
        style: {
            base: {
                iconColor: '#FFF',
                color: '#FFF',
                fontWeight: '300',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                    color: '#CFD7E0'
                }
            }
        }
    };

    public cartElements: CartElement[] = [];
    private subscription: Subscription = new Subscription();
    public user: User;
    public customer: Customer;

    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any = [];
    public total: number = 0;

    constructor(
        private cartStateService: CartStateService,
        private router: Router,
        private userDataService: UserDataService,
        private paymentsDataService: PaymentsDataService,
        private ordersDataService: OrdersDataService,
        private stripeService: StripeService,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit(): void
    {
        //init form
        this.form = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.email]),
            first_name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            last_name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            phone_number: new FormControl(null, [Validators.required, Validators.maxLength(50), phoneNumber ]),
            postal_code: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
            address_line_1: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            address_line_2: new FormControl(null, [Validators.maxLength(50)]),
            city: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            country: new FormControl(null, [Validators.maxLength(50)]),
            token: new FormControl(null, )
        });
        //messages config
        const fieldsConfig = {
            email: ['required', 'email'],
            first_name: ['required', { key: "maxlength", value: 50}],
            last_name: ['required', { key: "maxlength", value: 50}],
            phone_number: ['required', { key: "maxlength", value: 50}, 'phoneNumber'],
            postal_code: ['required', { key: "maxlength", value: 30}],
            address_line_1: ['required', { key: "maxlength", value: 50}],
            address_line_2: [{ key: "maxlength", value: 50}],
            city: ['required', { key: "maxlength", value: 50}],
            country: [{ key: "maxlength", value: 50}]
        };
        this.messages = getMessagesConfig(fieldsConfig);

        //getting user and customer data
        this.userDataService
            .getUser()
            .pipe(flatMap((user: User) => {
                this.user = user;
                this.form.patchValue({
                    email: user.email,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    phone_number: user.phone_number,
                    postal_code: user.address.postal_code,
                    address_line_1: user.address.address_line_1,
                    address_line_2: user.address.address_line_2,
                    city: user.address.city,
                    country: user.address.country
                });

                return this.paymentsDataService.getCustomer(user._id);
            }))
            .subscribe((customer: Customer) => this.customer = customer);

        this.subscription.add(this.cartStateService
            .cartElementsSubscription
            .subscribe((elements: CartElement[]) => {
                if(!elements.length)
                    return this.router.navigate(["/cart"]);
                this.cartElements = elements;
                this.total = this.cartElements.reduce((total, element) => total + (element.amount * element.product.price),0);
            }));
    }

    ngOnDestroy(): void
    {
        if(this.subscription)
            this.subscription.unsubscribe();
    }

    public onResetCardSelection(): void
    {
        this.form.get("token").reset();
    }
    public onDeleteCard(card: Card): void
    {
        this.inProgress = true;
        this.paymentsDataService
            .deleteCard(card._id)
            .subscribe(() => {
                this.inProgress = false;
                this.snackBar.open('Card deleted!');
                this.paymentsDataService
                    .getCustomer(this.user._id)
                    .subscribe((customer: Customer) => this.customer = customer);
            }, () => this.inProgress = false)
    }

    public onSubmit(): void
    {
        //form validity
        if(!this.form.valid)
            return this.form.markAsTouched();

        this.inProgress = true;
        if(!this.form.get("token").value)
        {
            this.stripeService
                .createToken(this.card.element)
                .subscribe((result) => {
                    if (result.token)
                    {
                       this.form.get("token").patchValue(result.token.id);
                       this.submitOrder();
                    }
                    else
                        this.inProgress = false;
                });
        }
        else
            this.submitOrder();
    }

    private submitOrder(): void
    {
        const params = {
            ...this.form.value,
            items: JSON.stringify(this.cartElements.map(element => {
                return {
                    amount: element.amount,
                    product: element.product._id
                }
            }))
        }

        this.ordersDataService
            .addOrder(params)
            .subscribe(result => {
                this.inProgress = false;
                this.cartStateService.clearCart();
                this.snackBar.open('Successfully placed new order!');
                this.router.navigate(["/summary", result._id]);
            })
    }
}
