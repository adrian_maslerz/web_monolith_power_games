import { Component, OnDestroy, OnInit } from '@angular/core';
import { CartStateService } from '../../../services/state/cart.state.service';
import { CartElement } from '../../../interfaces/cart-element.interface';
import { Subscription } from 'rxjs';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { getMessagesConfig } from '../../../utilities/form.utils';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/core/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { ModalLoginComponent } from '../../components/feature/modal-login/modal-login.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-cart',
    templateUrl: './cart.component.html',
    styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit, OnDestroy
{
    public cartElements: CartElement[] = [];
    private subscription: Subscription = new Subscription();

    public form: FormGroup;
    public messagesPacks: any = [];
    public total: number = 0;

    constructor(
        private cartStateService: CartStateService,
        private authService: AuthService,
        private router: Router,
        private dialog: MatDialog,
        private snackBar: MatSnackBar
    ) { }

    ngOnInit(): void
    {
        //init form
        this.form = new FormGroup({
            elements: new FormArray([])
        });

        this.subscription.add(this.cartStateService
            .cartElementsSubscription
            .subscribe((elements: CartElement[]) => {
                this.cartElements = elements;
                this.createElementsForm(elements);
                this.total = this.cartElements.reduce((total, element) => total + (element.amount * element.product.price),0);
            }));
    }

    ngOnDestroy(): void
    {
        if(this.subscription)
            this.subscription.unsubscribe();
    }

    public onToPayment(): void
    {
        if(this.authService.isAuthenticated())
        {
            if(this.cartElements.length)
                this.router.navigate(["/payment"]);
            else
                this.snackBar.open("No items in cart!", "X", { panelClass: "error-snackbar" });
        }
        else
            this.dialog.open(ModalLoginComponent);
    }

    public onDelete(element: CartElement): void
    {
        this.cartStateService.deleteCartElement(element);
    }
    public onDeleteAll(): void
    {
        this.cartStateService.clearCart();
    }

    private createElementsForm(elements: CartElement[]): void
    {
        (<FormArray>this.form.get("elements")).clear();
        this.messagesPacks = [];
        elements.forEach(element => {
            const form = new FormGroup({
                amount: new FormControl(element.amount, [Validators.required, Validators.min(1), Validators.max(element.product.availability)]),
                product: new FormControl(element.product._id)
            });
            form.markAllAsTouched();
            (<FormArray>this.form.get("elements")).push(form);

            this.subscription.add(form.valueChanges
                .subscribe(data => {
                    const element = this.cartElements.find(element => element.product._id == data.product);
                    element.amount = data.amount;
                    if(form.valid)
                        this.cartStateService.updateCartElementAmount(element);
                })
            );

            //messages config
            const fieldsConfig = {
                amount: ['required', 'numeric', { key: "min", value: 1}, { key: "max", value: element.product.availability}],
            };
            const messages = getMessagesConfig(fieldsConfig);
            this.messagesPacks.push(messages);
        })
    }
}
