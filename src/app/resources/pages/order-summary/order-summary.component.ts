import { Component, OnInit } from '@angular/core';
import { Order } from '../../../interfaces/order.interface';
import { OrdersDataService } from '../../../services/data/orders.data.service';
import { ActivatedRoute } from '@angular/router';
import { flatMap } from 'rxjs/operators';
import { PaymentsDataService } from '../../../services/data/payments.data.service';
import { Payment } from '../../../interfaces/payment.interface';

@Component({
    selector: 'app-order-summary',
    templateUrl: './order-summary.component.html',
    styleUrls: ['./order-summary.component.scss'],
    providers: [ OrdersDataService, PaymentsDataService ]
})
export class OrderSummaryComponent implements OnInit
{
    public order: Order;
    public payment: Payment;
    private id: string = "";

    constructor(
        private route: ActivatedRoute,
        private ordersDataService: OrdersDataService,
        private paymentsDataService: PaymentsDataService
    ) { }

    ngOnInit(): void
    {
        //getting route param
        this.id = this.route.snapshot.params['id'];

        //getting order
        this.ordersDataService
            .getOrder(this.id)
            .pipe(flatMap((order: Order) => {
                this.order = order;
                return this.paymentsDataService.getPayment(this.order.payment);
            }))
            .subscribe((payment: Payment) => this.payment = payment);
    }

}
