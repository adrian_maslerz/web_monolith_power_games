import { Component, OnInit } from '@angular/core';
import { ProductsDataService } from '../../../../services/data/products.data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../../../../interfaces/product.interface';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { getMessagesConfig } from '../../../../utilities/form.utils';
import { CartStateService } from '../../../../services/state/cart.state.service';
import { CartElement } from '../../../../interfaces/cart-element.interface';
import { MatDialog } from '@angular/material/dialog';
import { ModalProductAddedSuccessComponent } from '../../../components/feature/modal-product-added-success/modal-product-added-success.component';

@Component({
    selector: 'app-game-details',
    templateUrl: './game-details.component.html',
    styleUrls: ['./game-details.component.scss']
})
export class GameDetailsComponent implements OnInit
{
    public id: string = '';
    public product: Product;

    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any = [];

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private dialog: MatDialog,
        private productsDataService: ProductsDataService,
        private cartStateService: CartStateService
    ) { }

    ngOnInit(): void
    {
        this.id = this.route.snapshot.params[ 'id' ];

        //init form
        this.form = new FormGroup({
            amount: new FormControl(null, [Validators.required, Validators.min(1), Validators.max(1)]),
        });

        //messages config
        const fieldsConfig = {
            amount: ['required', 'numeric', { key: "min", value: 1}, { key: "max", value: 1}],
        };
        this.messages = getMessagesConfig(fieldsConfig);

        this.getData();
    }

    private getData(): void
    {
        this.productsDataService
            .getProduct(this.id)
            .subscribe((product: Product) => {
                this.product = product;
                this.form.get("amount").setValidators([Validators.required, Validators.min(1), Validators.max(product.availability)]);
                const fieldsConfig = {
                    amount: ['required', 'numeric', { key: "min", value: 1}, { key: "max", value: product.availability}],
                };
                this.messages = getMessagesConfig(fieldsConfig);
            });
    }

    public onSubmit(): void
    {
        if (!this.form.valid)
            return this.form.markAsTouched();

        const element: CartElement = {
            amount: this.form.get("amount").value,
            product: this.product
        }
        this.cartStateService.addCartElement(element);
        this.dialog.open(ModalProductAddedSuccessComponent);
    }
}
