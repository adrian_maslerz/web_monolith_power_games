import { Component, OnInit } from '@angular/core';
import { ProductsDataService } from '../../../services/data/products.data.service';
import { Product } from '../../../interfaces/product.interface';
import { OrdersDataService } from '../../../services/data/orders.data.service';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ModalForgotPasswordChangeComponent } from '../../components/feature/modal-forgot-password-change/modal-forgot-password-change.component';
import { User } from '../../../interfaces/user.interface';
import { ProductCopy } from '../../../interfaces/product-copy.interface';
import { Review } from '../../../interfaces/review.interface';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    providers: [ ProductsDataService, OrdersDataService ]
})
export class HomeComponent implements OnInit
{
    public carouselItems: { image: string, title: string, description: string }[] = [
        {
            image: "wallpaper-1.jpg",
            title: "Assassin's Creed Valhalla",
            description: "Assassin's Creed Valhalla is an open world action-adventure game, structured around several main story quests and numerous optional side missions. The player takes on the role of Eivor a Viking raider, as they lead their fellow Vikings against the Anglo-Saxon kingdoms."
        },
        {
            image: "wallpaper-2.jpg",
            title: "Need for Speed Heat",
            description: "Need for Speed Heat is a racing game set in an open world environment called Palm City, a fictionalised version of the city of Miami, Florida and its surrounding area. The in-game map features diverse geography, including mountainous areas and open fields."
        },
        {
            image: "wallpaper-3.jpg",
            title: "The Last of Us Part II",
            description: "The Last of Us Part II is an action-adventure game played from a third-person perspective. The player traverses post-apocalyptic environments such as buildings and forests to advance the story. The player can use firearms, improvised weapons, and stealth to defend against hostile humans and cannibalistic creatures infected by a mutated strain of the Cordyceps fungus."
        },
        {
            image: "wallpaper-4.jpg",
            title: "Cyberpunk 2077",
            description: "Cyberpunk 2077 is played in a first-person perspective as V, a mercenary whose voice face, hairstyles, body type and modifications, background, and clothing are customisable. Stat categories—Body, Intelligence, Reflexes, Technical, and Cool—are influenced by the character classes that players assume, which are NetRunner (hacking), Techie (machinery), and Solo (combat)."
        },
        {
            image: "wallpaper-5.jpg",
            title: "Halo Infinite",
            description: "According to Microsoft, Master Chief returns in Halo Infinite with his greatest adventure yet to save humanity. The storyline of Halo Infinite will be much more human, with the Master Chief playing a more central role than in Halo 5: Guardians."
        }
    ];
    public newest: Product[] = [];
    public premieres: Product[] = [];
    public recommended: Product[] = [];
    public reviews: { user: User, product: ProductCopy, review: Review }[] = [];

    constructor(
        private productsDataService: ProductsDataService,
        private ordersDataService: OrdersDataService,
        private route: ActivatedRoute,
        private dialog: MatDialog
    ) { }

    ngOnInit(): void
    {
        this.getData();
        if(this.route.snapshot.url.map(segment => segment.path).includes("password-reset"))
            this.dialog.open(ModalForgotPasswordChangeComponent);
    }

    private getData(): void
    {
        this.productsDataService
            .getHomeProducts()
            .subscribe((results) => {
                this.newest = results.newest;
                this.premieres = results.premieres;
                this.recommended = results.recommended;
            });

        this.ordersDataService
            .getOrdersReviews()
            .subscribe((results: { reviews: { user: User, product: ProductCopy, review: Review }[]}) => this.reviews = results.reviews);
    }

    public rates(to: number): any[]
    {
        const data = [];
        for(let i = 0 ; i < to; i++)
            data.push(i);
        return data;
    }
}
