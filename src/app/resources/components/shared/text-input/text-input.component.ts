import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { handleValidationErrorMessage, handleValidationStateClass } from '../../../../utilities/form.utils';

@Component({
    selector: 'app-text-input',
    templateUrl: './text-input.component.html',
    styleUrls: ['./text-input.component.scss']
})
export class TextInputComponent
{
    @Input() form: FormGroup;
    @Input() name: string;
    @Input() displayName: string = this.name;
    @Input() messages: any[] = [];
    @Input() type: string = "text";
    @Input() light: boolean = false;

    public formUtils = { handleValidationStateClass, handleValidationErrorMessage };

    constructor() { }
}
