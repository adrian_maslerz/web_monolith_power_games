import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent
{
    @Input() width: string = "500px";
    @Input() title: string = "";

    constructor() { }

}
