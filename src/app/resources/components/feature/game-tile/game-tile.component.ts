import { Component, Input } from '@angular/core';
import { Product } from '../../../../interfaces/product.interface';

@Component({
    selector: 'app-game-tile',
    templateUrl: './game-tile.component.html',
    styleUrls: ['./game-tile.component.scss']
})
export class GameTileComponent
{
    @Input() product: Product;
    constructor() { }
}
