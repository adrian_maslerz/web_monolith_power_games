import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AuthDataService } from '../../../../services/data/auth.data.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { getMessagesConfig } from '../../../../utilities/form.utils';
import { ModalLoginComponent } from '../modal-login/modal-login.component';
import { equalToFieldValue, password, phoneNumber } from '../../../../utilities/validators';

@Component({
    selector: 'app-modal-register',
    templateUrl: './modal-register.component.html',
    styleUrls: ['./modal-register.component.scss'],
    providers: [ AuthDataService ]
})
export class ModalRegisterComponent implements OnInit {

    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any = [];

    constructor(
        private dialogRef: MatDialogRef<ModalRegisterComponent>,
        private dialog: MatDialog,
        private authDataService: AuthDataService,
        private snackBar: MatSnackBar
    ) {}

    ngOnInit()
    {
        //init form
        this.form = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.email]),
            password: new FormControl(null, [Validators.required, Validators.minLength(8), password]),
            repeated_password: new FormControl(null, [Validators.required]),
            first_name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            last_name: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            phone_number: new FormControl(null, [Validators.required, Validators.maxLength(50), phoneNumber ]),
            postal_code: new FormControl(null, [Validators.required, Validators.maxLength(30)]),
            address_line_1: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            address_line_2: new FormControl(null, [Validators.maxLength(50)]),
            city: new FormControl(null, [Validators.required, Validators.maxLength(50)]),
            country: new FormControl(null, [Validators.maxLength(50)]),
        });

        //messages config
        const fieldsConfig = {
            email: ['required', 'email'],
            password: ['required', { key: 'minlength', value: 8 }, 'password', { key: 'equalToFieldValue', message: 'Passwords don\'t match.' }],
            repeated_password: ['required', { key: 'equalToFieldValue', message: 'Passwords don\'t match.' }],
            first_name: ['required', { key: "maxlength", value: 50}],
            last_name: ['required', { key: "maxlength", value: 50}],
            phone_number: ['required', { key: "maxlength", value: 50}, 'phoneNumber'],
            postal_code: ['required', { key: "maxlength", value: 30}],
            address_line_1: ['required', { key: "maxlength", value: 50}],
            address_line_2: [{ key: "maxlength", value: 50}],
            city: ['required', { key: "maxlength", value: 50}],
            country: [{ key: "maxlength", value: 50}]
        };
        this.messages = getMessagesConfig(fieldsConfig);

        // password
        this.form.get('password')
            .valueChanges
            .subscribe(
                () =>
                {
                    const control = this.form.get('repeated_password');
                    control.setValidators([Validators.required, equalToFieldValue(this.form.get('password').value)]);
                    control.updateValueAndValidity();
                });

        this.form.get('repeated_password')
            .valueChanges
            .subscribe(() => this.form.get('repeated_password').setValidators([Validators.required, equalToFieldValue(this.form.get('password').value)]));
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.authDataService
            .register(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.snackBar.open('Successfully registered new account!');
                    this.dialogRef.close();
                },
                () =>
                {
                    this.inProgress = false;
                });
    }

    public onLoginModal(): void
    {
        this.dialogRef.close();
        this.dialog.open(ModalLoginComponent);
    }
}
