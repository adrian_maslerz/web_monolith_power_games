import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AuthDataService } from '../../../../services/data/auth.data.service';
import { getMessagesConfig } from '../../../../utilities/form.utils';
import { ModalLoginComponent } from '../modal-login/modal-login.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
    selector: 'app-modal-forgot-password',
    templateUrl: './modal-forgot-password.component.html',
    styleUrls: ['./modal-forgot-password.component.scss'],
    providers: [ AuthDataService ]
})
export class ModalForgotPasswordComponent implements OnInit
{
    public form: FormGroup;
    public inProgress: boolean = false;
    public messages: any = [];

    constructor(
        private dialogRef: MatDialogRef<ModalForgotPasswordComponent>,
        private dialog: MatDialog,
        private authDataService: AuthDataService,
        private snackBar: MatSnackBar
    ) {}

    ngOnInit()
    {
        //init form
        this.form = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.email]),
            role: new FormControl("USER")
        });

        //messages config
        const fieldsConfig = {
            email: ['required', 'email']
        };
        this.messages = getMessagesConfig(fieldsConfig);
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.authDataService
            .resetPassword(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.snackBar.open('Instructions have been sent to your email address!');
                    this.dialogRef.close();
                },
                () =>
                {
                    this.inProgress = false;
                });
    }

    public onLoginModal(): void
    {
        this.dialogRef.close();
        this.dialog.open(ModalLoginComponent);
    }
}
