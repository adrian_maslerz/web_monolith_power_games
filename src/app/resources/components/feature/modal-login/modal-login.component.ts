import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { getMessagesConfig, handleValidationErrorMessage, handleValidationStateClass } from '../../../../utilities/form.utils';
import { AuthDataService } from '../../../../services/data/auth.data.service';
import { ModalForgotPasswordComponent } from '../modal-forgot-password/modal-forgot-password.component';
import { ModalRegisterComponent } from '../modal-register/modal-register.component';

@Component({
    selector: 'app-modal-login',
    templateUrl: './modal-login.component.html',
    styleUrls: ['./modal-login.component.scss'],
    providers: [ AuthDataService ]
})
export class ModalLoginComponent implements OnInit
{
    public form: FormGroup;
    public inProgress: boolean = false;
    public formUtils = { handleValidationStateClass, handleValidationErrorMessage };
    public messages = [];

    constructor(
        private dialogRef: MatDialogRef<ModalLoginComponent>,
        private authDataService: AuthDataService,
        private dialog: MatDialog
    ) { }

    ngOnInit(): void
    {
        //init form
        this.form = new FormGroup({
            email: new FormControl(null, [Validators.required, Validators.email]),
            password: new FormControl(null, Validators.required),
            role: new FormControl("USER")
        });

        //messages config
        const fieldsConfig = {
            email: ['required', 'email'],
            password: ['required']
        };
        this.messages = getMessagesConfig(fieldsConfig);
    }

    public onSubmit(): void
    {
        //handle invalid
        if (!this.form.valid)
            return this.form.markAllAsTouched();

        this.inProgress = true;
        this.authDataService
            .login(this.form.value)
            .subscribe(() =>
                {
                    this.inProgress = false;
                    this.dialogRef.close();
                },
                () => {
                    this.inProgress = false;
                });
    }

    public onForgotPasswordModal(): void
    {
        this.dialogRef.close();
        this.dialog.open(ModalForgotPasswordComponent);
    }

    public onRegisterModal(): void
    {
        this.dialogRef.close();
        this.dialog.open(ModalRegisterComponent);
    }
}
