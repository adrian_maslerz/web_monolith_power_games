import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-modal-product-added-success',
    templateUrl: './modal-product-added-success.component.html',
    styleUrls: ['./modal-product-added-success.component.scss']
})
export class ModalProductAddedSuccessComponent
{
    constructor(
        public dialogRef: MatDialogRef<ModalProductAddedSuccessComponent>,
    ) { }
}
