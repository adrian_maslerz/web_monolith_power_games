import { AfterViewInit, Component, Input, OnInit, ViewChild } from '@angular/core';
import { of } from 'rxjs';
import { delay, take } from 'rxjs/operators';
import { MatStepper } from '@angular/material/stepper';

@Component({
    selector: 'app-order-stepper',
    templateUrl: './order-stepper.component.html',
    styleUrls: ['./order-stepper.component.scss']
})
export class OrderStepperComponent implements AfterViewInit
{
    @ViewChild("stepper") stepper: MatStepper;
    @Input() step: number = 1;

    constructor() { }

    ngAfterViewInit(): void
    {
        of({}).pipe(take(1), delay(200)).subscribe(() =>  this.stepper.selectedIndex = this.step - 1)
    }
}
