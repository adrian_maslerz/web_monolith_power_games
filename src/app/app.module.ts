import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GamesComponent } from './resources/pages/games/games.component';
import { GamesListComponent } from './resources/pages/games/games-list/games-list.component';
import { GameDetailsComponent } from './resources/pages/games/game-details/game-details.component';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { CategoriesWidgetComponent } from './resources/components/feature/categories-widget/categories-widget.component';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './resources/pages/home/home.component';
import { ContactComponent } from './resources/pages/contact/contact.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './resources/components/shared/modal/modal.component';
import { ModalLoginComponent } from './resources/components/feature/modal-login/modal-login.component';
import { ModalRegisterComponent } from './resources/components/feature/modal-register/modal-register.component';
import { ModalForgotPasswordComponent } from './resources/components/feature/modal-forgot-password/modal-forgot-password.component';
import { ModalForgotPasswordChangeComponent } from './resources/components/feature/modal-forgot-password-change/modal-forgot-password-change.component';
import { MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule } from '@angular/material/dialog';
import { TextInputComponent } from './resources/components/shared/text-input/text-input.component';
import { HttpBasicInterceptor } from './interceptors/http.basic.interceptor';
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule } from '@angular/material/snack-bar';
import { GameTileComponent } from './resources/components/feature/game-tile/game-tile.component';
import { HeaderComponent } from './resources/components/feature/header/header.component';
import { FooterComponent } from './resources/components/feature/footer/footer.component';
import { TextareaInputComponent } from './resources/components/shared/textarea-input/textarea-input.component';
import { CartComponent } from './resources/pages/cart/cart.component';
import { PaymentComponent } from './resources/pages/payment/payment.component';
import { OrderSummaryComponent } from './resources/pages/order-summary/order-summary.component';
import { MatStepperModule } from '@angular/material/stepper';
import { OrderStepperComponent } from './resources/components/feature/order-stepper/order-stepper.component';
import { NgxStripeModule } from 'ngx-stripe';
import { environment } from '../environments/environment';
import { MatRadioModule } from '@angular/material/radio';
import { LoggedComponent } from './resources/pages/logged/logged.component';
import { ProfileComponent } from './resources/pages/logged/profile/profile.component';
import { ProfileDetailsComponent } from './resources/pages/logged/profile/profile-details/profile-details.component';
import { ChangeEmailComponent } from './resources/pages/logged/profile/change-email/change-email.component';
import { EditProfileComponent } from './resources/pages/logged/profile/edit-profile/edit-profile.component';
import { ChangePasswordComponent } from './resources/pages/logged/profile/change-password/change-password.component';
import { OrdersComponent } from './resources/pages/logged/orders/orders.component';
import { OrdersListComponent } from './resources/pages/logged/orders/orders-list/orders-list.component';
import { OrderDetailsComponent } from './resources/pages/logged/orders/order-details/order-details.component';
import { TableSortComponent } from './resources/components/shared/table-sort/table-sort.component';
import { MatBadgeModule } from '@angular/material/badge';
import { SelectInputComponent } from './resources/components/shared/select-input/select-input.component';
import { ModalProductAddedSuccessComponent } from './resources/components/feature/modal-product-added-success/modal-product-added-success.component';

@NgModule({
    declarations: [
        AppComponent,
        GamesComponent,
        GamesListComponent,
        GameDetailsComponent,
        CategoriesWidgetComponent,
        HomeComponent,
        ContactComponent,
        ModalComponent,
        ModalLoginComponent,
        ModalRegisterComponent,
        ModalForgotPasswordComponent,
        ModalForgotPasswordChangeComponent,
        TextInputComponent,
        TextareaInputComponent,
        GameTileComponent,
        HeaderComponent,
        FooterComponent,
        CartComponent,
        PaymentComponent,
        OrderSummaryComponent,
        OrderStepperComponent,
        LoggedComponent,
        ProfileComponent,
        ProfileDetailsComponent,
        ChangeEmailComponent,
        EditProfileComponent,
        ChangePasswordComponent,
        OrdersComponent,
        OrdersListComponent,
        OrderDetailsComponent,
        TableSortComponent,
        SelectInputComponent,
        ModalProductAddedSuccessComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatButtonModule,
        MatToolbarModule,
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatCardModule,
        MatGridListModule,
        MatRippleModule,
        InfiniteScrollModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        MatDialogModule,
        MatSnackBarModule,
        MatStepperModule,
        NgxStripeModule.forRoot(environment.stripe_key),
        MatRadioModule,
        MatBadgeModule
    ],
    providers: [
        {
            provide: MAT_DIALOG_DEFAULT_OPTIONS,
            useValue: { position: { top: "100px"}, hasBackdrop: true, width: "450px" }
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpBasicInterceptor,
            multi: true
        },
        {
            provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
            useValue: { duration: 2500, horizontalPosition: "right", verticalPosition: "bottom", panelClass: "success-snackbar" }
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}
