import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { AuthService } from '../services/core/auth.service';
import { catchError } from 'rxjs/operators';
import { getMessageError } from '../utilities/form.utils';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class HttpBasicInterceptor implements HttpInterceptor
{

    constructor(private authService: AuthService, private snackBar: MatSnackBar) {}

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>
    {
        //setting JSON content
        const headersConfig = {};
        if (req.body && req.body.constructor && req.body.constructor.name !== 'FormData')
            headersConfig[ 'Content-Type' ] = 'application/json';

        //adding token
        const token = this.authService.isAuthenticated() ? this.authService.getAuthToken().token : null;
        if (token)
            headersConfig['X-Access-Token'] = token;


        const request = req.clone({ setHeaders: headersConfig });
        return next.handle(request)
            .pipe(
                catchError(
                    (error: HttpErrorResponse) => {
                        const message = getMessageError(error);
                        if(message)
                            this.snackBar.open(message, "X", { panelClass: "error-snackbar" });
                        return throwError(error);
                    }
                )
            );
    }

}
