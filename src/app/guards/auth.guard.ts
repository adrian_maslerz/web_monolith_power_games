import { Injectable } from '@angular/core';
import { CanActivate, Router, CanLoad, Route } from '@angular/router';

import { Observable } from 'rxjs';
import { AuthService } from '../services/core/auth.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad
{

    constructor(private authService: AuthService, private router: Router) { }

    canActivate(): Observable<boolean> | Promise<boolean> | boolean
    {
        if(!this.authService.isAuthenticated())
        {
            this.router.navigate([ '/' ]);
            return false;
        }

        return true
    }

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean
    {
        return this.canActivate();
    }
}
