import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { map } from 'rxjs/internal/operators';
import { SuccessInterface } from '../../interfaces/success.interface';
import { AuthService } from '../core/auth.service';
import { User } from '../../interfaces/user.interface';

@Injectable()
export class UserDataService
{
    constructor(private apiService: ApiService, private authService: AuthService) { }

    public getUser() : Observable<User>
    {
        return this.apiService.get("users","/user/profile").pipe(map((data: User) => {

            //updating user state
            this.authService.updateLoggedUser(data);
            return data;
        }));
    }

    public updateUser(data: any) : Observable<SuccessInterface>
    {
        return this.apiService.put("users", "/user/profile", data);
    }

    public changePassword(data: any) : Observable<SuccessInterface>
    {
        return this.apiService.put("users", "/user/password", data);
    }

    public changeEmail(data: any) : Observable<SuccessInterface>
    {
        return this.apiService.put("users", "/user/email", data);
    }

    public contactMessage(data: any) : Observable<SuccessInterface>
    {
        return this.apiService.post("users", "/contact", data);
    }
}
