import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { Pagination } from '../../interfaces/pagination.interface';
import { Order } from '../../interfaces/order.interface';
import { Review } from '../../interfaces/review.interface';
import { User } from '../../interfaces/user.interface';
import { ProductCopy } from '../../interfaces/product-copy.interface';

@Injectable()
export class OrdersDataService
{
    constructor(private apiService: ApiService) { }

    public getOrders(data: any) : Observable<Pagination<Order>>
    {
        return this.apiService.get("orders","/orders", data);
    }

    public getOrder(id: string) : Observable<Order>
    {
        return this.apiService.get("orders", "/orders/" + id);
    }

    public getOrdersReviews() : Observable<{ reviews: {user: User, product: ProductCopy, review: Review}[]}>
    {
        return this.apiService.get("orders","/orders/reviews");
    }

    public addOrder(data: any) : Observable<any>
    {
        return this.apiService.post("orders","/orders", data);
    }

    public addOrderItemReview(orderId: string, itemId: string, data: any) : Observable<any>
    {
        return this.apiService.post("orders","/orders/" + orderId + "/items/" + itemId, data);
    }
}
