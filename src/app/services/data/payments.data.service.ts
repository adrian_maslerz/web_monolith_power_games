import { Injectable } from '@angular/core';
import { ApiService } from '../core/api.service';
import { Observable } from 'rxjs/index';
import { Payment } from '../../interfaces/payment.interface';
import { Customer } from '../../interfaces/customer.interface';

@Injectable()
export class PaymentsDataService
{
    constructor(private apiService: ApiService) { }

    public getPayment(id: string) : Observable<Payment>
    {
        return this.apiService.get("payments", "/payments/" + id);
    }

    public getCustomer(id: string) : Observable<Customer>
    {
        return this.apiService.get("payments", "/customers/" + id);
    }

    public deleteCard(id: string) : Observable<Customer>
    {
        return this.apiService.delete("payments", "/cards/" + id);
    }
}
