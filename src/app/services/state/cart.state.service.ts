import { Injectable } from '@angular/core';
import { CartElement } from '../../interfaces/cart-element.interface';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CartStateService
{
    private storage = localStorage;
    private storageKey: string = "cart";
    private cartElements: CartElement[] = [];
    private cartElementsSubject: BehaviorSubject<CartElement[]> = new BehaviorSubject([]);
    public cartElementsSubscription: Observable<CartElement[]> = this.cartElementsSubject.asObservable();

    constructor()
    {
        var cart: CartElement[];
        try
        {
            cart = JSON.parse(this.storage.getItem(this.storageKey));
            if(cart)
            {
                this.cartElements = cart;
                this.cartElementsSubject.next(this.cartElements);
            }
        }
        catch (error) {}
    }

    public addCartElement(element: CartElement): void
    {
        const el = this.cartElements.find(el => el.product._id == element.product._id);
        if(!el)
        {
            this.cartElements.push(element);
            this.cartElementsSubject.next(this.cartElements);
            this.storage.setItem(this.storageKey, JSON.stringify(this.cartElements));
        }
    }

    public updateCartElementAmount(element: CartElement): void
    {
        const el = this.cartElements.find(el => el.product._id == element.product._id);
        if(el)
        {
            el.amount = element.amount;
            this.cartElementsSubject.next(this.cartElements);
            this.storage.setItem(this.storageKey, JSON.stringify(this.cartElements));
        }
    }

    public deleteCartElement(element: CartElement): void
    {
        const el = this.cartElements.find(el => el.product._id == element.product._id);
        if(el)
        {
            this.cartElements = this.cartElements.filter(el => el.product._id != element.product._id)
            this.cartElementsSubject.next(this.cartElements);
            this.storage.setItem(this.storageKey, JSON.stringify(this.cartElements));
        }
    }

    public clearCart(): void
    {
        this.cartElements = [];
        this.storage.setItem(this.storageKey, JSON.stringify(this.cartElements));
        this.cartElementsSubject.next(this.cartElements);
    }
}
