import { Card } from './card.interface';

export interface Customer
{
    _id: string;
    stripe_id: string;
    user: string;
    email: string;
    created: number;
    cards: Card[];
}
